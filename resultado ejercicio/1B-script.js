
function iniciar() {
    const btnInsertar = document.querySelector('#btn-insertar');

    btnInsertar.onclick = function (e) {
        const nombre = document.getElementById('nombre');
        const apellido = document.getElementById('apellido');
        const edad = document.getElementById('edad');

        insertarLi(nombre.value, apellido.value, edad.value);
    };
}

function insertarLi(nombre, apellido, edad) {
    let ul = document.getElementsByName('actualizame')[0];

    let li = document.createElement('li');
    let btn = document.createElement('button');

    if (!(nombre && apellido && edad)) {
        alert('No no no...');
        return;
    }

    btn.innerText = 'Borrar';

    btn.onclick = function (event) {
        event.target.parentElement.remove();
    }

    li.innerHTML = `${apellido}, ${nombre} (${edad}) `;
    // li.innerHTML = apellido + ',' + nombre + ' - ' + edad;

    li.append(btn);

    ul.append(li);
}

window.addEventListener('load', iniciar);